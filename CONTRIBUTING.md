<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->

## We Welcome your Contribution!

- Please open **an issue**, if you have improvement ideas.
- Please check the [developer guide](docs/src/developer-guide.rst) if you want to contribute an improvement.
- If it is the first time that you contribute, please make sure to:
  - Mark your copyright (or the copyright of your research organization / company) using the SPDX `SPDX-FileCopyrightText` tag
  - Add yourself to the list of contributors in the [README](README.md#contributors)
